package smartest.automation.academy.testng;

import org.testng.annotations.*;

@Listeners(value = {TestNGListener.class})
public class MyParentConfiguration {
    public static final String ACCOUNTS = "accounts";
    public static final String PAYMENTS = "payments";

    @BeforeSuite(alwaysRun = true)
    public void beforeSuite() {
        System.out.println("BeforeSuite");
    }

    @BeforeTest(alwaysRun = true)
    public void beforeTest() {
        System.out.println("BeforeTest");
    }

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        System.out.println("BeforeClass");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        System.out.println("AfterClass");
        //throw new RuntimeException("Exception in after class");
    }

    @AfterMethod(alwaysRun = true)
    public void terminateDriver() {
        System.out.println("Terminating driver...");
    }

    @AfterTest(alwaysRun = true)
    public void afterTest() {
        System.out.println("AfterTest");
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite() {
        System.out.println("AfterSuite");
    }
}
