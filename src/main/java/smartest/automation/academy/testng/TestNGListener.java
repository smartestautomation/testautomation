package smartest.automation.academy.testng;

import org.testng.ISuiteListener;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestNGListener implements ITestListener, ISuiteListener {

    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("onTestStart_" + result.getMethod().getMethodName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        this.sendResultToJira(result.getMethod().getMethodName(), "SUCCESS");
    }

    @Override
    public void onTestFailure(ITestResult result) {

        this.sendResultToJira(result.getMethod().getMethodName(), "FAILED");
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        this.sendResultToJira(result.getMethod().getMethodName(), "SKIPPED");
    }

    private void sendResultToJira(String testName, String result) {
        System.out.println(String.format("Test [%s] was [%s]", testName, result));
    }
}
