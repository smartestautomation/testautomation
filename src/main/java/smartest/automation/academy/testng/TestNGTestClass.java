package smartest.automation.academy.testng;

import org.testng.annotations.*;

@Listeners(value = {TestNGListener.class})
public class TestNGTestClass {
    public static final String SMOKE = "smoke";

    @BeforeSuite(alwaysRun = true)
    public void suiteSetup() {
        System.out.println("suiteSetup");
    }

    @BeforeTest(alwaysRun = true)
    public void testSetup() {
        System.out.println("testSetup");
    }

    @AfterTest(alwaysRun = true)
    public void resetTest() {
        System.out.println("resetTest");
    }

    @AfterSuite(alwaysRun = true)
    public void resetSuite() {
        System.out.println("resetSuite");
    }
}
