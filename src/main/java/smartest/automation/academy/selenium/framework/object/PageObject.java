package smartest.automation.academy.selenium.framework.object;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import smartest.automation.academy.selenium.framework.driver.Driver;

public abstract class PageObject {
    protected final Driver driver;

    public PageObject(Driver driver) {
        this.driver = driver;
        // wait until page is loaded
        this.driver.synchronization();
        // initialize all WebElement fields inside PageObject
        PageFactory.initElements(driver.getDriver(), this);
    }

    protected WebElement waitForElement(WebElement element) {
        return this.driver.waitForElement(element);
    }

    protected WebElement waitForElementEnabled(WebElement element) {
        return this.driver.waitForElementEnabled(element);
    }

    protected void verifyValues(String expected, String actual) {
        if (!expected.equals(actual)) {
            this.driver.addException(new RuntimeException(String.format("Expected value [%s] does not equal to actual value [%s]", expected, actual)));
        }
    }
}
