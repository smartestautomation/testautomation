package smartest.automation.academy.selenium.framework.test;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import smartest.automation.academy.selenium.framework.driver.Driver;
import smartest.automation.academy.selenium.framework.listeners.TestListener;

@Listeners(value = {TestListener.class})
public class TestClass {
    protected Driver driver;

    {
        System.setProperty("webdriver.chrome.driver", "C:\\tools\\chromedriver.exe");
    }

    @BeforeClass
    public void initDriver() {
        this.driver = new Driver();
    }

    public Driver getDriver() {
        return driver;
    }

    @AfterClass
    public void reset() {
        if (this.driver != null) {
            this.driver.reset();
        }
    }
}