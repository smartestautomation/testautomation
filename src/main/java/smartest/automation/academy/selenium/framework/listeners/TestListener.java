package smartest.automation.academy.selenium.framework.listeners;

import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;
import smartest.automation.academy.selenium.framework.test.TestClass;

public class TestListener implements ITestListener {

    @Override
    public void onTestFailure(ITestResult result) {
        TestClass testConfiguration = (TestClass) result.getInstance();
        testConfiguration.getDriver().takeScreenshot();
        processExceptionContainer(result);
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        processExceptionContainer(result);
    }

    private void processExceptionContainer(ITestResult result) {
        TestClass testConfiguration = (TestClass) result.getInstance();
        RuntimeException exception = testConfiguration.getDriver()
                                                      .getExceptionContainer();
        if (!result.isSuccess()) {
            exception.addSuppressed(result.getThrowable());
        }
        if (exception.getSuppressed().length != 0) {
            result.setStatus(ITestResult.FAILURE);
        }
        result.setThrowable(exception);
    }
}
