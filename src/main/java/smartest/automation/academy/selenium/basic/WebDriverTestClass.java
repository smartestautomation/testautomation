package smartest.automation.academy.selenium.basic;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class WebDriverTestClass {
    protected Map<String, String> windowMap = new HashMap<>();
    protected ChromeOptions options = new ChromeOptions();
    protected RemoteWebDriver driver;

    {
        this.options.addArguments("--remote-allow-origins=*");
        System.setProperty("webdriver.chrome.driver", "C:\\tools\\chromedriver.exe");
    }

    @AfterMethod
    public void reset() throws InterruptedException {
        Thread.sleep(5000);
        this.driver.quit();
    }

    public WebElement findElement(By by, Duration duration) {
        waitForJQueriesDone(this.driver);
        waitForPageToLoad(this.driver);

        WebDriverWait wait = new WebDriverWait(this.driver, duration, Duration.ofSeconds(2));
        ExpectedCondition<WebElement> expectedCondition = (d) -> {
            if (d == null) {
                return null;
            }
            try {
                WebElement element = d.findElement(by);
                if (element.isEnabled() && element.isDisplayed()) {
                    return element;
                }
            } catch (NoSuchElementException | StaleElementReferenceException e) {
                return null;
            }
            return null;
        };
        return wait.until(expectedCondition);
    }

    public void click(By by, Duration timeout) {
        WebDriverWait wait = new WebDriverWait(this.driver, timeout, Duration.ofSeconds(2));
        ExpectedCondition<Boolean> expectedCondition = (d) -> {
            if (d == null) {
                return null;
            }
            try {
                d.findElement(by)
                 .click();
                return true;
            } catch (NoSuchElementException | StaleElementReferenceException | ElementNotInteractableException e) {
                return false;
            }
        };
        wait.until(expectedCondition);
    }

    public WebElement findElement(By by) {
        return this.findElement(by, Duration.ofSeconds(60));
    }

    private void waitForPageToLoad(RemoteWebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20), Duration.ofSeconds(1));
        ExpectedCondition<Boolean> expectedCondition = (d) -> driver.executeScript("return document.readyState;")
                                                                    .equals("complete");
        wait.until(expectedCondition);
    }

    private void waitForJQueriesDone(RemoteWebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        ExpectedCondition<Boolean> expectedCondition = (d) -> {
            if ((Boolean) driver.executeScript("return window.jQuery === undefined;")) {
                return true;
            }
            try {
                return (Boolean) driver.executeScript("return jQuery.active === 0;");
            } catch (JavascriptException e) {
                return false;
            }
        };
        wait.until(expectedCondition);
    }
}
