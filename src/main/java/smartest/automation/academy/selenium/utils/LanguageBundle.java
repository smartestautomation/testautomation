package smartest.automation.academy.selenium.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class LanguageBundle {

    public static String translateValue(String value) {
        ResourceBundle resourceBundle = getResourceBundle();
        if (resourceBundle == null) {
            return value;
        }
        try {
            return new String(resourceBundle.getString(value).getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);
        } catch (MissingResourceException exception) {
            return value;
        }
    }

    private static ResourceBundle getResourceBundle() {
        URLClassLoader classLoader = LanguageBundle.getURLClassLoader();
        try {
            return ResourceBundle.getBundle("language", LanguageBundle.getLanguageLocale(), classLoader);
        } catch (MissingResourceException e) {
            return null;
        }
    }

    private static URLClassLoader getURLClassLoader() {
        Path path = Paths.get("/test/resources/");
        try {
            return new URLClassLoader(new URL[] {path.toUri().toURL()});
        } catch (MalformedURLException e) {
            throw new RuntimeException(String.format("Path to language bundle directory is not valid: %s", path));
        }
    }

    private static Locale getLanguageLocale() {
        String language = System.getProperty("language");
        String country = System.getProperty("country");

        if (language == null) {
            return Locale.getDefault();
        }
        if (country == null) {
            return new Locale(language);
        }
        return new Locale(language, country);
    }
}
