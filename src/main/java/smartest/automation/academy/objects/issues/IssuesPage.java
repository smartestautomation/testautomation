package smartest.automation.academy.objects.issues;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import smartest.automation.academy.objects.Header;
import smartest.automation.academy.selenium.framework.driver.Driver;
import smartest.automation.academy.selenium.framework.object.PageObject;

import java.util.List;

/**
 * Stranka Ukoly
 */
public class IssuesPage extends Header {

    @FindBy(xpath = "//tr[contains(@id, 'issue')]")
    private List<WebElement> issueRow;

    @FindBy(className = "pages")
    private WebElement pages;

    public IssuesPage(Driver driver) {
        super(driver);
    }

    public IssueDetailPage openIssueDetail(String issueId) {
        this.findIssue(issueId).findElement(By.className("id")).click();
        return new IssueDetailPage(this.driver);
    }

    private WebElement findIssue(String issueId) {
        for (WebElement issueRow: this.issueRow) {
            if (issueRow.findElement(By.xpath(".//td[@class='id']//a")).getText().equals(issueId)) {
                return issueRow;
            }
        }
        try {
            this.driver.getDriver().executeScript("arguments[0].scrollIntoView();", this.pages);
            this.pages.findElement(By.xpath(".//a[@accesskey='n']")).click();
        } catch (NoSuchElementException e) {
            throw new RuntimeException("Issue row not found");
        }
        return this.findIssue(issueId);
    }
}
