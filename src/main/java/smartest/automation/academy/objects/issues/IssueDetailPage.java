package smartest.automation.academy.objects.issues;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import smartest.automation.academy.objects.Header;
import smartest.automation.academy.selenium.framework.driver.Driver;
import smartest.automation.academy.selenium.framework.object.PageObject;

public class IssueDetailPage extends Header {

    @FindBy(xpath = "//*[@class='attributes']//*[contains(@class,'status')]//*[@class='value']")
    private WebElement issueState;

    @FindBy(xpath = "//*[@class='attributes']//*[contains(@class,'priority')]//*[@class='value']")
    private WebElement issuePriority;

    @FindBy(xpath = "//*[@class='attributes']//*[contains(@class,'category')]//*[@class='value']")
    private WebElement issueCategory;

    public IssueDetailPage(Driver driver) {
        super(driver);
    }

    public IssueDetailPage verifyIssueState(String expectedState) {
        this.verifyValues(expectedState, this.issueState.getText());
        return this;
    }

    public IssueDetailPage verifyIssuePriority(String expectedState) {
        this.verifyValues(expectedState, this.issueState.getText());
        return this;
    }

    public IssueDetailPage verifyIssueCategory(String expectedState) {
        this.verifyValues(expectedState, this.issueState.getText());
        return this;
    }
}
