package smartest.automation.academy.objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import smartest.automation.academy.objects.issues.IssuesPage;
import smartest.automation.academy.objects.plan.PlanPage;
import smartest.automation.academy.selenium.framework.driver.Driver;
import smartest.automation.academy.selenium.framework.object.PageObject;

public class Header extends PageObject {

    @FindBy(xpath = "//a[@href='/projects/redmine/roadmap']")
    private WebElement planLink;

    @FindBy(xpath = "//a[@href='/projects/redmine/issues']")
    private WebElement issuesLink;

    public Header(Driver driver) {
        super(driver);
    }

    public IssuesPage clickOnIssues() {
        this.waitForElement(this.issuesLink).click();
        return new IssuesPage(this.driver);
    }

    public PlanPage clickOnPlan() {
        this.waitForElement(this.planLink).click();
        return new PlanPage(this.driver);
    }
}
