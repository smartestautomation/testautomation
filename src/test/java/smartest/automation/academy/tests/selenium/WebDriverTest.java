package smartest.automation.academy.tests.selenium;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.Browser;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import smartest.automation.academy.selenium.basic.WebDriverTestClass;
import smartest.automation.academy.selenium.utils.LanguageBundle;

import javax.swing.event.CellEditorListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.List;

public class WebDriverTest extends WebDriverTestClass {
    private final RuntimeException exceptionContainer = new RuntimeException("Exceptions found during the test:");

    @Test(enabled = false)
    public void findElementTest_01() {
        this.driver = new ChromeDriver(this.options);
        this.driver.get("https://redmine.org");

        this.driver.manage()
                   .window()
                   .maximize();

        // IMPLICITLY WAIT
        // wait until element present, but not visible or clickable!
        this.driver.manage()
                   .timeouts()
                   .implicitlyWait(Duration.ofSeconds(5));
        // wait until page is loaded
        this.driver.manage()
                   .timeouts()
                   .pageLoadTimeout(Duration.ofSeconds(20));
        // wait until js function ends
        this.driver.manage()
                   .timeouts()
                   .scriptTimeout(Duration.ofSeconds(20));

        // if there is no element found, exception will be thrown
        WebElement element = this.driver.findElement(By.xpath("//a[@href='/login']"));
        element.click();
    }

    @Test(enabled = false)
    public void findElementsTest_02() {
        this.driver = new ChromeDriver(this.options);
        this.driver.get("https://redmine.org/projects/redmine/issues");

        // IMPLICITLY WAIT
        // wait until element present, but not visible or clickable!
        this.driver.manage()
                   .timeouts()
                   .implicitlyWait(Duration.ofSeconds(5));
        // wait until page is loaded
        this.driver.manage()
                   .timeouts()
                   .pageLoadTimeout(Duration.ofSeconds(20));
        // wait until js function ends
        this.driver.manage()
                   .timeouts()
                   .scriptTimeout(Duration.ofSeconds(20));

        // if there is no element found, empty list is returned
        List<WebElement> elements = this.driver.findElements(By.xpath("//tr[contains(@id, 'issues')]"));
        if (elements.isEmpty()) {
            throw new RuntimeException("");
        }
        for (WebElement element: elements) {
            System.out.println("Verifying element is displayed: " + element);
            Assert.assertTrue(element.isDisplayed());
        }
        System.out.println(elements.size());
    }

    @Test(enabled = false)
    public void waitForElementTest_03() {
        this.driver = new ChromeDriver(this.options);
        this.driver.get("https://redmine.org");

        this.findElement(By.xpath("//a[@href='/login']"), Duration.ofSeconds(3)).click();
    }

    @Test(enabled = false)
    public void elementApisTest_04() {
        this.driver = new ChromeDriver(this.options);
        this.driver.get("https://redmine.org");

        WebElement element = this.findElement(By.xpath("//a[@href='/login']"), Duration.ofSeconds(10));
        System.out.println(element.getText());
        System.out.println(element.isEnabled());
        System.out.println(element.isDisplayed());
        System.out.println(element.getAttribute("href"));
        System.out.println(element.getLocation()
                                  .toString());
    }

    @Test(enabled = false)
    public void findElementInContextTest_05() {
        this.driver = new ChromeDriver(this.options);
        this.driver.get("https://redmine.org/projects/redmine/issues");

        WebElement issuesTable = this.findElement(By.xpath("//table[contains(@class,'issues')]"), Duration.ofSeconds(10));

        WebElement tableRow = issuesTable.findElement(By.tagName("tr"));
        //WebElement tableRow = this.findElement(By.tagName("tr"), Duration.ofSeconds(10), issuesTable);
        System.out.println(tableRow.getText());

        tableRow = issuesTable.findElement(By.xpath(".//tr"));
        //tableRow = this.findElement(By.xpath("//tr"), Duration.ofSeconds(10), issuesTable);
        System.out.println(tableRow.getText());
    }

    @Test(enabled = false)
    public void exceptionContainer_06() {
        this.driver = new ChromeDriver(this.options);
        this.driver.get("https://redmine.org");

        WebElement element = this.findElement(By.xpath("//a[@href='/login']"));
        try {
            System.out.println(LanguageBundle.translateValue("LOGIN"));
            this.verifyText("LOGIN", element.getText());
        } finally {
            if (this.exceptionContainer.getSuppressed().length != 0) {
                throw this.exceptionContainer;
            }
        }
    }

    private void verifyText(String expected, String actual) {
        if (!LanguageBundle.translateValue(expected)
                           .equals(actual)) {
            this.exceptionContainer.addSuppressed(
                    new RuntimeException(String.format("Expected text '%s' is different than actual '%s'", LanguageBundle.translateValue(expected), actual)));
        }
    }

    @Test(enabled = false)
    public void gridTest_07() throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setBrowserName(Browser.CHROME.browserName());
        desiredCapabilities.setPlatform(Platform.WINDOWS);

        System.out.println(System.getProperty("webdriver.chrome.driver"));

        this.options.merge(desiredCapabilities);
        this.driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), this.options);
        this.driver.get("https://redmine.org");
    }

    @Test(enabled = false)
    public void clickByJS_08() {
        this.driver = new ChromeDriver(this.options);
        this.driver.manage()
                   .window()
                   .maximize();
        this.driver.get("https://redmine.org");

        WebElement loginButton = findElement(By.xpath("//a[@href='/login']"));
        this.driver.executeScript("arguments[0].click();", loginButton);
    }

    @Test(enabled = false)
    public void scrollWindowToEnd_09() throws InterruptedException {
        this.driver = new ChromeDriver(this.options);
        // connect to website
        this.driver.get("https://tst.smartestautomation.tech/scrolls.html");
        // scroll down
        this.driver.executeScript("window.scrollTo(document.body.scrollLeft, document.body.scrollHeight)");
        Thread.sleep(2000);
        // scroll up
        this.driver.executeScript("window.scrollTo(document.body.scrollLeft, document.body.scrollTop)");
    }

    @Test(enabled = false)
    public void scrollIntoView_10() {
        this.driver = new ChromeDriver(this.options);
        // connect to website
        this.driver.get("https://tst.smartestautomation.tech/scrolls.html");
        WebElement footer = findElement(By.xpath("//*[@id='footer']"), Duration.ofSeconds(30));

        this.driver.executeScript("arguments[0].scrollIntoView();", footer);
    }

    @Test(enabled = false)
    public void scrollVertically_11() {
        this.driver = new ChromeDriver(this.options);
        // connect to website
        this.driver.get("https://tst.smartestautomation.tech/scrolls.html");

        WebElement table = findElement(By.xpath("//*[@id='vertical-scroll-table']"));
        this.driver.executeScript("arguments[0].scrollTo(0, arguments[0].scrollHeight)", table);
    }

    @Test(enabled = false)
    public void scrollHorizontally_12() {
        this.driver = new ChromeDriver(this.options);
        // connect to website
        this.driver.get("https://tst.smartestautomation.tech/scrolls.html");

        WebElement table = findElement(By.xpath("//*[@id='horizontal-scroll-table']"));
        this.driver.executeScript("arguments[0].scrollTo(arguments[0].scrollWidth, 0)", table);
    }

    @Test(enabled = false)
    public void scrollBy_13() {
        this.driver = new ChromeDriver(this.options);
        // connect to website
        this.driver.get("https://tst.smartestautomation.tech/scrolls.html");

        WebDriverWait wait = new WebDriverWait(this.driver, Duration.ofSeconds(20), Duration.ofSeconds(2));
        ExpectedCondition<WebElement> expectedCondition = (d) -> {
            try {
                WebElement element = driver.findElement(By.xpath("//*[@id='footer-modal']"));
                if (element.isDisplayed()) {
                    return element;
                }
                return null;
            } catch (NoSuchElementException e) {
                return null;
            } finally {
                this.driver.executeScript("window.scrollBy(arguments[0], arguments[1])", 50, 450);
            }
        };
        wait.until(expectedCondition);
    }

    @Test(enabled = false)
    public void multiTabTest_14() {
        this.driver = new ChromeDriver(this.options);
        // connect to website
        this.driver.get("https://seznam.cz");

        System.out.println(this.driver.getWindowHandle());
        windowMap.put("SEZNAM", this.driver.getWindowHandle());

        this.driver.switchTo()
                   .newWindow(WindowType.TAB);
        this.driver.get("https://google.cz");

        windowMap.put("GOOGLE", this.driver.getWindowHandle());

        System.out.println(windowMap);

        this.driver.switchTo()
                   .window(windowMap.get("SEZNAM"));

        this.driver.get("https://redmine.org");

        // closing browser window
        this.driver.close();

        this.driver.switchTo()
                   .window(windowMap.get("GOOGLE"));
        this.driver.get("https://redmine.org");
    }

    @Test(enabled = true)
    public void frameTest_15() {
        this.driver = new ChromeDriver(this.options);
        // connect to website
        this.driver.get("https://tst.smartestautomation.tech/frames.html");

        WebElement frame = findElement(By.xpath("//*[@name='frame_1']"));
        this.driver.switchTo()
                   .frame(frame);

        WebElement heading = findElement(By.xpath("//*[@id='test_module_heading']"), Duration.ofSeconds(5));
        System.out.println(heading.getText());

        this.driver.switchTo()
                   .defaultContent();

        System.out.println(findElement(By.xpath("//*[@id='test_module_heading']"), Duration.ofSeconds(5)).getText());
    }
}
