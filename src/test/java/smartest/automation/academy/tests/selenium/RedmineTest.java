package smartest.automation.academy.tests.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import smartest.automation.academy.selenium.basic.WebDriverTestClass;

import java.time.Duration;

public class RedmineTest extends WebDriverTestClass {
    private RemoteWebDriver driver;

    @BeforeClass
    public void setup() {
        // webdriver.gecko.driver
        // webdriver.firefox.bin

        // webdriver.ie.driver
        // webdriver.safari.driver
        // webdriver.edge.driver
        // initialization of chrome instance
        this.driver = new ChromeDriver(this.options);

        // connect to web address
        this.driver.get("https://redmine.org/");
        this.driver.manage()
                   .window()
                   .maximize();

        // IMPLICITLY WAIT
        // wait until element present
        this.driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        // wait until page is loaded
        this.driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(20));
        // wait until js function ends
        this.driver.manage().timeouts().scriptTimeout(Duration.ofSeconds(20));
    }

    @Test
    public void redmineTest() {
        WebElement issues = this.driver.findElement(By.xpath("//a[@href='/projects/redmine/issues']"));
        issues.click();

        WebElement issueRow = this.findIssue("40348");
        Assert.assertEquals(issueRow.findElement(By.className("tracker")).getText(), "Defect");
        Assert.assertEquals(issueRow.findElement(By.className("subject")).getText(), "Parent Task field not available in Import issues screen");

        issueRow.findElement(By.className("id")).click();

        WebElement status = this.driver.findElement(By.xpath("//*[@class='attributes']//*[contains(@class,'status')]//*[@class='value']"));
        WebElement priority = this.driver.findElement(By.xpath("//*[@class='attributes']//*[contains(@class,'priority')]//*[@class='value']"));

        Assert.assertEquals(status.getText(), "New");
        Assert.assertEquals(priority.getText(), "Normal");
    }

    public WebElement findIssue(String issueId) {
        for (WebElement issueRow: this.driver.findElements(By.xpath("//tr[contains(@id, 'issue')]"))) {
            if (issueRow.findElement(By.xpath(".//td[@class='id']//a")).getText().equals(issueId)) {
                return issueRow;
            }
        }
        try {
            WebElement pages = this.driver.findElement(By.className("pages"));
            this.driver.executeScript("arguments[0].scrollIntoView();", pages);
            pages.findElement(By.xpath(".//a[@accesskey='n']")).click();
        } catch (NoSuchElementException e) {
            throw new RuntimeException("Issue row not found");
        }
        return this.findIssue(issueId);
    }

    @AfterClass
    public void reset() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // close browser after test
        this.driver.quit();
    }
}
