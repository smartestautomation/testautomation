package smartest.automation.academy.tests.testng._01;

import org.testng.SkipException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class SimpleTest {
    private boolean isPreviousTestFailed = false;
    private final static String PAYMENTS = "payments";
    private final static String ACCOUNTS = "accounts";

    @Test(groups = {PAYMENTS})
    public void login() {
        try {
            System.out.println("login");
            //throw new RuntimeException("Login failed");
        } catch (Throwable e) {
            this.isPreviousTestFailed = true;
            throw e;
        }
        // exception container
    }

    @Test(groups = {PAYMENTS})
    public void createNewPayment() {
        try {
            System.out.println("createNewPayment");
        } catch (Throwable e) {
            this.isPreviousTestFailed = true;
            throw e;
        }
    }

    @Test(groups = {PAYMENTS})
    public void verifyPaymentSent() {
        System.out.println("verifyPaymentSent");
    }

    @Test(groups = {ACCOUNTS})
    public void accountTest() {
        System.out.println("accountTest");
    }

    @BeforeMethod
    public void isPreviousTestFailed() {
        if (this.isPreviousTestFailed) {
            throw new SkipException("Previous test failed");
        }
    }
}
