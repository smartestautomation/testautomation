package smartest.automation.academy.tests.testng._02;

import org.testng.annotations.*;

public class ConfigurationsTest {
    private final static String PAYMENTS = "payments";

    @BeforeSuite(alwaysRun = true)
    public void beforeSuite() {
        System.out.println("Before suite");
    }

    @BeforeTest
    public void beforeTest() {
        System.out.println("Before test");
    }

    @BeforeClass(groups = PAYMENTS)
    public void beforeClass() {
        System.out.println("Before class");
    }

    @BeforeMethod(groups = PAYMENTS)
    public void setup() {
        System.out.println("setup");
    }

    @Test(groups = PAYMENTS)
    public void domesticPaymentsTest() {
        System.out.println("domesticPaymentsTest: " + Thread.currentThread()
                                                            .getId());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        //throw new RuntimeException("Domestic payment failure");
    }

    @Test(groups = PAYMENTS)
    public void foreignPaymentTest() {
        System.out.println("foreignPaymentTest: " + Thread.currentThread()
                                                          .getId());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @AfterMethod(groups = PAYMENTS)
    public void reset() {
        System.out.println("reset");
    }

    @AfterClass(groups = PAYMENTS)
    public void afterClass() {
        System.out.println("After class");
    }
}