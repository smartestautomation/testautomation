package smartest.automation.academy.tests.testng._04;

import org.testng.annotations.Test;

public class DependentTest {

    @Test
    public void createNewPaymentTest() {
        System.out.println("createNewPaymentTest: " + Thread.currentThread().getId());
        //throw new RuntimeException("Payment not created");
    }

    @Test(dependsOnMethods = {"createNewPaymentTest"})
    public void verifyPaymentSentTest() {
        System.out.println("verifyPaymentSentTest: " + Thread.currentThread().getId());
    }

    @Test
    public void verifyAccount() {
        System.out.println("VerifyAccount: " + Thread.currentThread().getId());
    }
}