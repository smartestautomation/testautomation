package smartest.automation.academy.tests.testng._05;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DataProviderTest {

    @DataProvider(name = "accounts")
    public Object[][] accounts() {
        return new String[][] {
                {"123", "Honza"},
                {"897", "Petr"},
                {"785", "Roman"}};
    }

    @Test(dataProvider = "accounts")
    public void verifyAccountDetail(String accountNumber, String ownerName) {
        System.out.println(accountNumber + ":" + ownerName);
    }
}
