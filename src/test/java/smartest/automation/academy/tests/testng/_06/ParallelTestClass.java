package smartest.automation.academy.tests.testng._06;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ParallelTestClass {

    @BeforeClass
    @Parameters(value = {"browser", "version", "env", "platform", "language", "run"})
    public void seleniumSetup(
            @Optional(value = "CHROME") String browser,
            @Optional(value = "20") int version,
            @Optional(value = "UAT") String env,
            @Optional(value = "WINDOWS") String platform,
            String language,
            String run
    ) {
        System.out.println(browser + ", " + version + ", " + env + ", " + platform + ", " + language + ", " + run);
        switch (browser) {
            case "CHROME":
                System.out.println("Starting chrome");
                break;
            case "FIREFOX":
                System.out.println("Starting firefox");
                break;
            default:
                throw new RuntimeException("");
        }
    }

    @Test
    public void test1() {
        System.out.println("test1: " + Thread.currentThread().getId());
    }

    @Test
    public void test2() {
        System.out.println("test2: " + Thread.currentThread().getId());
    }

    @Test
    public void test3() {
        System.out.println("test3: " + Thread.currentThread().getId());
    }
}
