package smartest.automation.academy.tests.testng._03;

import org.testng.annotations.*;
import smartest.automation.academy.testng.MyParentConfiguration;


public class ParentConfigurationTest extends MyParentConfiguration {

    @Override
    @BeforeClass(groups = PAYMENTS)
    public void beforeTest() {
        System.out.println("Annotation changed from BeforeTest to BeforeClass");
        // ignore
    }

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("BeforeMethod");
        //throw new RuntimeException("Exception in configuration");
    }

    @Test(timeOut = 1000L * 60 * 10, groups = {PAYMENTS})
    public void createNewDomesticPaymentTest() {
        System.out.println("createNewDomesticPaymentTest");
        //throw new RuntimeException("Test failed");
    }

    @Test(groups = {ACCOUNTS})
    public void test_2() {
        System.out.println("Test 2");
    }

    @AfterMethod(groups = {PAYMENTS})
    public void afterMethod() {
        System.out.println("AfterMethod");
        //throw new RuntimeException("Exception in configuration");
    }
}
