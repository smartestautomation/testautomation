package smartest.automation.academy.tests.redmine;

import org.testng.annotations.Test;
import smartest.automation.academy.objects.HomePage;
import smartest.automation.academy.selenium.framework.test.TestClass;

public class RedmineIssueTest extends TestClass {

    @Test
    public void verifyIssueState() {
        HomePage homePage = new HomePage(this.driver);
        homePage.clickOnIssues()
                .openIssueDetail("40403")
                .verifyIssueState("New")
                .verifyIssueCategory("Issuesss")
                .verifyIssuePriority("Normal")
                .clickOnIssues()
                .openIssueDetail("")
                .verifyIssuePriority("");
    }
}
